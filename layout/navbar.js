console.log("We here");

var navBarPromise = fetch('layout/nav-bar.html');
var navBarElement = document.getElementById('nav-bar');

navBarPromise.then( (response) => {
    return response.text();
}).catch ( (ex) => {
    console.log("Nav Bar HTML file is not valid." + ex);
})
.then ( (navBarPartial) => {
    console.log("Fetch done successfully !");
    navBarElement.insertAdjacentHTML('afterend', navBarPartial);
}).catch ( (ex) => {
    console.log("We have an error loading nav bar: " + ex);
}) 