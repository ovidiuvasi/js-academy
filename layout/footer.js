console.log("We here");

var navBarPromise = fetch('layout/footer.html');
var navBarElement = document.getElementById('footer');

navBarPromise.then( (response) => {
    return response.text();
}).catch ( (ex) => {
    console.log("Footer HTML file is not valid." + ex);
})
.then ( (footerPartial) => {
    console.log("Fetch done successfully !");
    navBarElement.insertAdjacentHTML('afterend', footerPartial);
}).catch ( (ex) => {
    console.log("We have an error: " + ex);
}) 