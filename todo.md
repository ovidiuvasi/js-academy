# Tema 1: Management articole (produse metro)

Se va implementa o aplicatie web (html, css & js) care sa acopere urmatoare nevoi de business ale domeniului si va contine:

- O pagina de homepage unde sa se descrie contextual aplicatiei si beneficiile acesteia

- Un formular de contact (formularul nu va avea si o functionalitate implementata – de trimitere de email ci va fi doar validat)

- Paginile aplicatiei vor urma un layout comun format din meniu, footer, etc

Se va urmari implementarea urmatoarelor aspecte:

- Folosirea tuturor tag-urilor HTML invatate

- Folosirea tuturor regulilor de CSS invatate