var articles = [
    {
        image: 'images/product.jpg',
        description: 'Desc product ONE',
        price: 'more details about product ONE'
    },
    {
        image: 'images/product.jpg',
        description: 'Desc product TWO',
        price: 'more details about product TWO'
    },
    {
        image: 'images/product.jpg',
        description: 'Desc product THREE',
        price: 'more details about product THREE'
    },
    {
        image: 'images/product.jpg',
        description: 'Desc product FOUR',
        price: 'more details about product FOUR'
    },
    {
        image: 'images/product.jpg',
        description: 'Desc product FIVE',
        price: 'more details about product FIVE'
    },
]