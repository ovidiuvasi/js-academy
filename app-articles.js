var btnLoadArticles = document.querySelector('.load-articles');

var articlesList = document.getElementsByClassName('products')[0];
var index = 0;

var modal = document.querySelector('.modal');
var backdrop = document.querySelector('.backdrop');
var openModalBtn = document.querySelector('.add-article');
var cancelModalBtn = document.querySelector('.modal-action--cancel');
var saveModalBtn = document.querySelector('.modal-action--save');
var updateModalBtn = document.querySelector('.modal-action--update');


openModalBtn.addEventListener('click', function() {
    modal.classList.add('open');
    backdrop.classList.add('open');
    document.querySelector('.modal-title').innerHTML = "Add Product Form";
});

function addArticle (article) {
    console.log(article);
    console.log(typeof(article));
    var divProductCard = document.createElement('div');
    divProductCard.className = 'product-card';
    divProductCard.id = index;

    var divProductImage = document.createElement('div');
    divProductImage.className = 'product-image';

    var imgTag = document.createElement('img');
    imgTag.setAttribute("src", article.image);

    var divProductInfo = document.createElement('div');
    divProductInfo.className = 'product-info';

    var h5Tag = document.createElement('h5');
    var h5TagCotent = document.createTextNode(article.description);
    h5Tag.appendChild(h5TagCotent);

    var h6Tag = document.createElement('h6');
    var h6TagCotent = document.createTextNode(article.price);
    h6Tag.appendChild(h6TagCotent);

    //TODO: rename class names
    var updateBtn = document.createElement('button');
    var updateBtnMessage = document.createTextNode('Update');
    updateBtn.appendChild(updateBtnMessage);
    updateBtn.classList = 'modal-action--save updateButton';
    updateBtn.id = index;
    updateBtn.setAttribute("onClick", "openModalForUpdateBtn(event)");

    var deleteBtn = document.createElement('button');
    var deleteBtnMessage = document.createTextNode('Delete');
    deleteBtn.id = index++;
    deleteBtn.classList = 'modal-action--cancel deleteButton';
    deleteBtn.setAttribute("onClick", "deleteArticle(event)");
    deleteBtn.appendChild(deleteBtnMessage);

    divProductImage.appendChild(imgTag);

    divProductInfo.appendChild(h5Tag);
    divProductInfo.appendChild(h6Tag);
    divProductInfo.appendChild(updateBtn);
    divProductInfo.appendChild(deleteBtn);

    divProductCard.appendChild(divProductImage);
    divProductCard.appendChild(divProductInfo);

    articlesList.appendChild(divProductCard);
}

function addArticles () {
    articles.forEach(article => {
        addArticle(article);
    });
}

//load articles when click on page
document.onload = addArticles();

if (cancelModalBtn) {
    cancelModalBtn.addEventListener('click', closeModalWindow);
}

function closeModalWindow() {
    if (modal) {
        modal.classList.remove("open");
    }
    backdrop.classList.remove("open");
}


function openModalForUpdateBtn(event) {
    modal.classList.add('open');
    var productInfoId = document.getElementById(event.target.id).id;
    modal.id = productInfoId;
    console.log(productInfoId);
    backdrop.classList.add('open');
    document.querySelector('.modal-title').innerHTML = "Update Product Form";
    
    saveModalBtn.classList.add("modal-action--update");
    saveModalBtn.addEventListener("click", updateData);
}

function getObjectFromInputFields() {
    //get data from all inputs
    var formInputs = document.querySelectorAll(".form-input");

    var formData = {};
    //populate object
    if (formInputs) {
        formInputs.forEach(input => {
            formData[input.id] = input.value;
        });
    }

    return formData;
}

function saveModalForm() {
    var objectToBeSaved = getObjectFromInputFields();    
    //add article to array
    articles.push(objectToBeSaved);
    //make html out of it
    addArticle(objectToBeSaved);
}

function updateData(event) {
    var formData = getObjectFromInputFields();
    
    var allProductCards = document.querySelectorAll(".product-card");
    var productCardForUpdateImage = allProductCards[modal.id];
    
    var imgSrcValue = productCardForUpdateImage.getElementsByTagName("img")[0];
    imgSrcValue.setAttribute("src", formData.image);
    
    var allProductInfos = document.querySelectorAll(".product-info");
    var productInfoForUpdateTexts = allProductInfos[modal.id];

    productInfoForUpdateTexts.getElementsByTagName("h5")[0].innerText = formData.description;
    productInfoForUpdateTexts.getElementsByTagName("h6")[0].innerText = formData.price;

    saveModalBtn.classList.remove("modal-action--update");
}

function deleteArticle (event) {
    var element = document.getElementById(event.target.id);
    //TODO delete also from array
    element.parentNode.removeChild(element);
} 

var classListForSaveButton = document.querySelector('.modal-action--save').className.split(/\s+/);
var isUpdate = false;

for (var i = 0; i < classListForSaveButton.length; i++) {
    if (classListForSaveButton[i] == "modal-action--update") {
        isUpdate = true;
        break;
    }
}

if (!isUpdate) {
    saveModalBtn.addEventListener('click', saveModalForm);
}
